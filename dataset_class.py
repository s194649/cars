from torch.utils.data import Dataset, DataLoader, random_split
#import cv2
from os.path import splitext
from os import listdir
import random
import logging
from numpy import save, load
import torch
from torchvision import transforms
import torchvision.transforms.functional as F
import random
import numpy as np


class BasicDataset(Dataset):
    def __init__(self, imgs_dir, ids, u=0.5, seed=42, num_cad=200,train=True,merge=True,tran=False):
        self.imgs_dir = imgs_dir
        self.merge = merge

        self.raw_ids = [splitext(file)[0] for file in listdir(imgs_dir) if not file.startswith('.')]
        
        #random.seed(seed)
        #random.shuffle(raw_ids)
        self.train = train
        self.ids = ids
        self.blur = transforms.GaussianBlur(5,2)
        
        cnt = 0
        self.mask_idx = [i for i in range(9) if i != 4]
        
        for i in self.raw_ids:
            if i[:4] in ['DOOR', 'OPEL']:
                if cnt >= num_cad:
                    pass
                else:
                    self.ids.append(i)
                    cnt += 1
            #else:
            #    self.ids.append(i)

        self.u = u
        
        #logging.info(f'Creating dataset with {len(self.ids)} examples')
        self.mean = [132.9940, 131.6884, 130.8786]
        self.std = [67.8263, 67.5548, 69.3866]
        self.transform = transforms.Normalize(self.mean,self.std)
        
        self.tran = tran
        self.mean_tran = [0.2577, 0.2561, 0.2582]
        self.std_tran = [0.3095, 0.3101, 0.3110]
        
        self.transform_tran = transforms.Normalize(self.mean_tran,self.std_tran)
        
        
    def segmentation_transform(self, img, mask):
        #i, j, h, w = transforms.RandomCrop.get_params(input, (100, 100))
        if self.train:
            degrees, translate, scale, shear = transforms.RandomAffine.get_params(degrees=[-20,20],
                                                                                  shears=[-20,20],
                                                                                  img_size=(256,256),
                                                                                  translate=None,
                                                                                  scale_ranges=None)
            img = transforms.functional.affine(img, degrees, translate, scale, shear,
                                               transforms.InterpolationMode.BILINEAR)
            mask = transforms.functional.affine(mask, degrees, translate, scale, shear,
                                                transforms.InterpolationMode.NEAREST)
            
            top,left,height,width = transforms.RandomResizedCrop.get_params(img,scale=(0.5,1),ratio=(1., 1.))
            size = (256,256)
            img = F.resized_crop(img, top, left, height, width,size,transforms.InterpolationMode.BILINEAR)
            mask = F.resized_crop(mask, top, left, height, width,size,transforms.InterpolationMode.NEAREST)
            ### greyscale !!!
            img = img/255
            img = transforms.ColorJitter(brightness=0.8,contrast=0.8,saturation=0.2,hue=0.3)(img)
            img = img*255
            
            if (random.random() > 0.5):
                img = F.hflip(img)
                mask = F.hflip(mask)
                
        mask[self.mask_idx] = self.blur(mask[self.mask_idx])
        mask[mask<0.5] = 0
        mask[mask>=0.5] = 1
        return img, mask
    
    def unnorm(self,img):
        MEAN = torch.tensor(self.mean)
        STD = torch.tensor(self.std)
        x = img * STD[:, None, None] + MEAN[:, None, None]
        return x
        
    def __len__(self):
        return len(self.ids)

    def __getitem__(self, i):
        idx = self.ids[i]
        
        #np_obj = glob(self.imgs_dir + idx + '.npy')
        np_obj = 'cleaner_data/' + idx + '.npy'
        
        img,mask = load(np_obj,allow_pickle=True)
        if self.merge:
            np_obj = 'car_mask/' + idx + '.pth'
            car_mask = torch.load(np_obj)
            img[~np.broadcast_to(car_mask,img.shape)] = 0
        img,mask = torch.from_numpy(img).type(torch.FloatTensor), torch.from_numpy(mask).type(torch.FloatTensor)
        img,mask = self.segmentation_transform(img,mask)
        if not self.tran:
            img = self.transform(img)
        else:
            img = img/255
            img = self.transform_tran(img)
        return img,mask