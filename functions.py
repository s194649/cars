#def unnorm(img):
#    img[0, :, :] = img[0, :, :]*0.229 + 0.485  
#    img[1, :, :] = img[1, :, :]*0.224 + 0.456  
#    img[2, :, :] = img[2, :, :]*0.225 + 0.406  
#    return img
import torch
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.cm as cm
import copy
import matplotlib.patheffects as PathEffects

def plot_eval(img,mask,y,classes,size,font_size,bg=9,norm=True):
    my_cmap = copy.copy(cm.jet)
    my_cmap.set_under('k', alpha=0)
    
    mask = torch.argmax(mask,axis=0)
    #img = unnorm(img)
    if norm:
        img = img / 255
    txt = []
    for i in torch.unique(mask):
        if not (i > len(classes) or i==bg):
            count = (mask==i).sum()
            txt.append([classes[int(i)],np.argwhere(mask==i).sum(1)/count])
    #fig = plt.figure(frameon=False)
    fig, (ax1, ax2) = plt.subplots(1, 2,figsize=size,frameon=False)
    ax1.set_title('Ground Truth')#'Ground Truth')
    ax2.set_title('Prediction')#'Prediction')
    ax1.axis([0, 256, 256, 0])
    ax2.axis([0, 256, 256, 0])
    for element in txt:
        pt = element[1]
        #print(int(pt[0]), int(pt[1]), element[0])
        ax1.text(int(pt[1]),int(pt[0]), element[0], fontsize=font_size,c='b',
                 path_effects=[PathEffects.withStroke(linewidth=3,
                                                      foreground="w")])
    ax1.imshow(img.transpose(0, 2).transpose(0, 1))
    ax1.imshow(mask,alpha=0.7,cmap=my_cmap, interpolation='none', clim=[1, 10])
    y = torch.argmax(y,axis=0)
    txt = []
    for i in torch.unique(y):
        if not (i > len(classes) or i==bg):
            count = (y==i).sum()
            txt.append([classes[int(i)],np.argwhere(y==i).sum(1)/count])
    #fig = plt.figure(frameon=False)
    for element in txt:
        pt = element[1]
        #print(int(pt[0]), int(pt[1]), element[0])
        ax2.text(int(pt[1]),int(pt[0]), element[0], fontsize=font_size,c='b',
                 path_effects=[PathEffects.withStroke(linewidth=3,
                                                      foreground="w")])
    ax2.imshow(img.transpose(0, 2).transpose(0, 1))
    ax2.imshow(y,alpha=0.7,cmap=my_cmap, interpolation='none', clim=[1, 10])
    ax1.axis('off')
    ax2.axis('off')
    fig.show()
    
#def unnorm(img):
#    img = img.detach().clone()
#    img[0, :, :] = (img[0, :, :] + 0.485 / 0.229)
#    img[1, :, :] = (img[1, :, :] + 0.456 / 0.224)
#    img[2, :, :] = (img[2, :, :] + 0.406 / 0.225)
#    return img