from torch.utils.data import Dataset, DataLoader, random_split
import cv2
from os.path import splitext
from os import listdir
import random
import logging
from numpy import save, load
import torch

class BasicDataset(Dataset):
    def __init__(self, imgs_dir, u=0.5, seed=42, num_cad=200):
        self.imgs_dir = imgs_dir

        raw_ids = [splitext(file)[0] for file in listdir(imgs_dir) if not file.startswith('.')]
        
        random.seed(seed)
        random.shuffle(raw_ids)
        
        self.ids = []
        
        cnt = 0
        
        for i in raw_ids:
            if i[:4] in ['DOOR', 'OPEL']:
                if cnt >= num_cad:
                    pass
                else:
                    self.ids.append(i)
                    cnt += 1
            else:
                self.ids.append(i)

        self.u = u
        
        logging.info(f'Creating dataset with {len(self.ids)} examples')

    def randomShiftScaleRotate(self, image, mask, target,
                               shift_limit=(-0.0625, 0.0625),
                               scale_limit=(-0.1, 0.1),
                               rotate_limit=(-45, 45), aspect_limit=(0, 0),
                               borderMode=cv2.BORDER_CONSTANT, u=0.5):

        image = image.transpose(1,2,0).copy()
        target = target.transpose(1,2,0).copy()

        if np.random.random() < u:
            height, width, channel = image.shape

            angle = np.random.uniform(rotate_limit[0], rotate_limit[1])  # degree
            scale = np.random.uniform(1 + scale_limit[0], 1 + scale_limit[1])
            aspect = np.random.uniform(1 + aspect_limit[0], 1 + aspect_limit[1])
            sx = scale * aspect / (aspect ** 0.5)
            sy = scale / (aspect ** 0.5)
            dx = round(np.random.uniform(shift_limit[0], shift_limit[1]) * width)
            dy = round(np.random.uniform(shift_limit[0], shift_limit[1]) * height)

            cc = np.math.cos(angle / 180 * np.math.pi) * sx
            ss = np.math.sin(angle / 180 * np.math.pi) * sy
            rotate_matrix = np.array([[cc, -ss], [ss, cc]])

            box0 = np.array([[0, 0], [width, 0], [width, height], [0, height], ])
            box1 = box0 - np.array([width / 2, height / 2])
            box1 = np.dot(box1, rotate_matrix.T) + np.array([width / 2 + dx, height / 2 + dy])

            box0 = box0.astype(np.float32)
            box1 = box1.astype(np.float32)
            mat = cv2.getPerspectiveTransform(box0, box1)
            image = cv2.warpPerspective(image, mat, (width, height), flags=cv2.INTER_LINEAR, borderMode=borderMode,
                                        borderValue=(0, 0, 0,))
            mask = cv2.warpPerspective(mask, mat, (width, height), flags=cv2.INTER_LINEAR, borderMode=borderMode,
                                       borderValue=(0, 0, 0,))
            target = cv2.warpPerspective(target, mat, (width, height), flags=cv2.INTER_LINEAR, borderMode=borderMode,
                                       borderValue=(0, 0, 0,))

        image = image.transpose(2,0,1).copy()
        target = target.transpose(2,0,1).copy()

        return image, mask, target

    def randomZoom(self, image, mask, target, zoom_limit=0.25, u=0.5):

        image = image.transpose(1,2,0).copy()
        target = target.transpose(1,2,0).copy()

        if np.random.random() < u:
            value = np.random.uniform(zoom_limit, 1)
            h, w = image.shape[:2]
            h_taken = int(value * h)
            w_taken = int(value * w)
            h_start = np.random.randint(0, h - h_taken)
            w_start = np.random.randint(0, w - w_taken)
            image = image[h_start:h_start + h_taken, w_start:w_start + w_taken, :]
            mask = mask[h_start:h_start + h_taken, w_start:w_start + w_taken]
            target = target[h_start:h_start + h_taken, w_start:w_start + w_taken, :]
            image = cv2.resize(image, (h, w), cv2.INTER_CUBIC)
            mask = cv2.resize(mask, (h, w), cv2.INTER_CUBIC)
            target = cv2.resize(target, (h, w), cv2.INTER_CUBIC)

        image = image.transpose(2,0,1).copy()
        target = target.transpose(2,0,1).copy()

        return image, mask, target

    def randomHorizontalFlip(self, image, mask, target, u=0.5):

        image = image.transpose(1,2,0).copy()
        target = target.transpose(1,2,0).copy()

        if np.random.random() < u:
            image = cv2.flip(image, 1)
            mask = cv2.flip(mask, 1)
            target = cv2.flip(target, 1)

        image = image.transpose(2,0,1).copy()
        target = target.transpose(2,0,1).copy()

        return image, mask, target
        
    def __len__(self):
        return len(self.ids)

    def __getitem__(self, i):
        idx = self.ids[i]
        
        #np_obj = glob(self.imgs_dir + idx + '.npy')
        np_obj = 'cleaner_data/' + idx + '.npy'
        
        data = load(np_obj,allow_pickle=True)#[0])
        print(data.dtype)
        #img = data[0:3]
        #target = data[3:-1]
        #mask = data[-1]

        img_aug, mask_aug, target_aug = img, target, mask
        
        #self.randomHorizontalFlip(img, mask, target, u=self.u)

        #img_aug, mask_aug, target_aug = self.randomShiftScaleRotate(img_aug, mask_aug, target_aug,
        #                                        shift_limit=(-0.1, 0.1),
        #                                        scale_limit=(-0.1, 0.1),
        #                                        aspect_limit=(-0.1, 0.1),
        #                                        rotate_limit=(-20, 20),
        #                                        u=self.u)

        #img_aug, mask_aug, target_aug = self.randomZoom(img_aug, mask_aug, target_aug,
        #                            zoom_limit=0.25,
        #                            u=self.u)
        
        #return {'image': torch.from_numpy(img_aug).type(torch.FloatTensor), 'mask': torch.from_numpy(mask_aug).squeeze(0).type(torch.FloatTensor), 'target_mask': torch.from_numpy(target_aug).type(torch.FloatTensor)}
        return torch.from_numpy(img_aug).type(torch.FloatTensor),torch.from_numpy(mask_aug).squeeze(0).type(torch.FloatTensor)#,torch.from_numpy(target_aug).type(torch.FloatTensor)