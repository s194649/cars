import torch
import torchvision
from pycocotools.coco import COCO
from PIL import Image
import os
from torchvision.transforms import ToTensor
import numpy as np
import random
from torchvision import transforms
import torchvision.transforms.functional as F


def segmentation_transform(img, mask):
    #img,mask = torch.from_numpy(img).type(torch.FloatTensor), torch.from_numpy(mask).type(torch.FloatTensor)
    #mask = torch.from_numpy(mask).type(torch.FloatTensor)
    v = random.gauss(0, 10)
    img = F.rotate(img,v,interpolation=transforms.InterpolationMode.NEAREST)
    mask = F.rotate(mask,v,interpolation=transforms.InterpolationMode.NEAREST)
    top,left,height,width = transforms.RandomResizedCrop.get_params(img,scale=(0.3,1),ratio=(1., 1.))
    size = (256,256)
    img = F.resized_crop(img, top, left, height, width,size,transforms.InterpolationMode.NEAREST)
    mask = F.resized_crop(mask, top, left, height, width,size,transforms.InterpolationMode.NEAREST)
    if (random.random() > 0.5):
        img = F.hflip(img)
        mask = F.hflip(mask)
    #img = transforms.ColorJitter(img)
    return img, mask

def load_anns(anns,cats,coco):
    (H,W) = coco.annToMask(anns[0]).shape
    mask = np.zeros([1,H,W])
    for ann in anns:
        mask += coco.annToMask(ann)
    mask[mask>1] = 1
    return mask


class fashpedia(torch.utils.data.Dataset):
    def __init__(self, root, annotation,train=True): 
        self.root = root
        self.coco = COCO(annotation)
        self.ids = list(sorted(self.coco.imgs.keys()))
        self.cats = self.coco.loadCats(self.coco.getCatIds())[0]['id']
        self.train = train
        self.mean = [0.4011, 0.3899, 0.3952]
        self.std = [0.2617, 0.2621, 0.2673]
        self.transform = transforms.Normalize(self.mean,self.std)

    def __getitem__(self, index):
        coco = self.coco
        img_id = self.ids[index]
        ann_ids = coco.getAnnIds(imgIds=img_id,catIds=self.cats)
        coco_annotation = coco.loadAnns(ann_ids)
        path = coco.loadImgs(img_id)[0]['file_name']
        img = Image.open(os.path.join(self.root, path))
        img = ToTensor()(img)
        mask = load_anns(coco_annotation,self.cats,coco)
        mask = torch.from_numpy(mask).type(torch.FloatTensor)
        if img.shape[0] != 3:
            print('Index:',index,' has ',img.shape[0], ' channels')
            return self.__getitem__(np.random.randint(0, len(self.ids)))
        if self.train:
            img, mask = segmentation_transform(img, mask)
        img = self.transform(img)
        return img, mask
        
    def __len__(self):
        return len(self.ids)
    
    def unnorm(self,img):
        MEAN = torch.tensor(self.mean)
        STD = torch.tensor(self.std)
        x = img * STD[:, None, None] + MEAN[:, None, None]
        return x