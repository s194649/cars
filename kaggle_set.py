import torch
import torchvision
from pycocotools.coco import COCO
from PIL import Image
import os
from torchvision.transforms import ToTensor
import numpy as np
import random
from torchvision import transforms
import torchvision.transforms.functional as F
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import DataLoader
from functions import *
import wandb

def segmentation_transform(img, mask):
    v = random.gauss(0, 10)
    img = F.rotate(img,v,interpolation=transforms.InterpolationMode.BILINEAR)
    mask = F.rotate(mask,v,interpolation=transforms.InterpolationMode.NEAREST)
    top,left,height,width = transforms.RandomResizedCrop.get_params(img,scale=(0.3,1),ratio=(1., 1.))
    size = (256,256)
    img = F.resized_crop(img, top, left, height, width,size,transforms.InterpolationMode.BILINEAR)
    mask = F.resized_crop(mask, top, left, height, width,size,transforms.InterpolationMode.NEAREST)
    #if (random.random() > 0.5):
    #    img = F.hflip(img)
    #    mask = F.hflip(mask)
    #img = self.transform(img)
    img = transforms.ColorJitter(brightness=0.8,contrast=0.8,saturation=0.2,hue=0.3)(img)
    return img, mask

def load_anns(anns,cats,coco):
    (H,W) = coco.annToMask(anns[0]).shape
    n = len(cats)
    mask = torch.zeros([n,H,W])
    for ann in anns:
        mask[ann['category_id'],:,:] += coco.annToMask(ann)
    #mask[mask>1] = 1
    return mask


class fashpedia(torch.utils.data.Dataset):
    def __init__(self, root, annotation, train=True, merge=False): 
        self.root = root
        self.coco = COCO(annotation)
        self.ids = list(sorted(self.coco.imgs.keys()))
        self.cats = self.coco.loadCats(self.coco.getCatIds())
        self.train = train
        self.merge = merge
        #self.mean = [0.2577, 0.2561, 0.2582]
        #self.std = [0.3095, 0.3101, 0.3110]
        self.mean = [0.4296, 0.4244, 0.4241]
        self.std = [0.3234, 0.3234, 0.3229]
        self.transform = transforms.Normalize(self.mean,self.std)
        

    def __getitem__(self, index):
        coco = self.coco
        img_id = self.ids[index]
        ann_ids = coco.getAnnIds(imgIds=img_id)#,catIds=self.cats)
        coco_annotation = coco.loadAnns(ann_ids)
        path = coco.loadImgs(img_id)[0]['file_name']
        img = Image.open(os.path.join(self.root, path))
        img = ToTensor()(img)
        mask = load_anns(coco_annotation,self.cats,coco)
        #mask = torch.from_numpy(mask).type(torch.FloatTensor)
        if img.shape[0] != 3:
            print('Index:',index,' has ',img.shape[0], ' channels')
            return self.__getitem__(np.random.randint(0, len(self.ids)))
        if self.merge:
            # Masking out background
            car_mask = torch.load(self.root + '/Untitled Folder/' + str(img_id) + '.pth' )
            img.numpy()[~np.broadcast_to(car_mask.unsqueeze(0),img.shape)] = 0
            mask[1] = car_mask & ~torch.any(mask, axis=0)
        if self.train:
            img, mask = segmentation_transform(img, mask)
        else:
            size = (256,256)
            img = transforms.functional.resize(img,size,transforms.InterpolationMode.BILINEAR)
            mask = transforms.functional.resize(mask,size,transforms.InterpolationMode.NEAREST)
        mask[0] = ~torch.any(mask, axis=0)
        
        img = self.transform(img)
        return img, mask
        
        
    def __len__(self):
        return len(self.ids)
    
    def unnorm(self,img):
        MEAN = torch.tensor(self.mean)
        STD = torch.tensor(self.std)
        x = img * STD[:, None, None] + MEAN[:, None, None]
        return x