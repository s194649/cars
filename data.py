import torch
import torchvision.transforms as transforms
import torch.utils.data as data
import os
import pickle
import numpy as np
import nltk
from PIL import Image
from pycocotools.coco import COCO


class CocoDataset(data.Dataset):
    """COCO Custom Dataset compatible with torch.utils.data.DataLoader."""
    def __init__(self, root, json,  transform=None):
        """Set the path for images, captions and vocabulary wrapper.
        
        Args:
            root: image directory.
            json: coco annotation file path.
            transform: image transformer.
        """
        self.root = root
        self.coco = COCO(json)
        self.ids = list(self.coco.imgs.keys())
        self.cats = self.coco.loadCats(self.coco.getCatIds())
        self.n = len(self.cats)
        self.transform = transform
        
    def load_anns(self, anns, cats, coco):
        (H,W) = coco.annToMask(anns[0]).shape
        mask = np.zeros([self.n,H,W])
        for ann in anns:
            mask[ann['category_id'],:,:] += coco.annToMask(ann)
        return mask

    def __getitem__(self, index):
        """Returns one data pair (image and caption)."""
        coco = self.coco
        img_id = self.ids[index]
        #ann_id = self.ids[index]
        #img_id = coco.anns[ann_id]['image_id']
        path = coco.loadImgs(img_id)[0]['file_name']
        ann_ids = coco.getAnnIds(imgIds=img_id)
        coco_annotation = coco.loadAnns(ann_ids)
        mask = self.load_anns(coco_annotation,self.cats,coco)
        mask = torch.as_tensor(mask, dtype=torch.float32)

        image = transforms.ToTensor()(Image.open(os.path.join(self.root, path)).convert('RGB'))
        if self.transform is not None:
            image = self.transform(image)
        return image, mask#, coco_annotation

    def __len__(self):
        return len(self.ids)
